# Samply MDR DAL

The Samply MDR Data Access Layer is a library that must be used to get elements from the MDR database.
It handles access rights accordingly to the database and user.

The layer distincts between:

- Elements (Dataelements, Dataelementgroup, Namespace, Record, Value Domain, etc.)
- Identified Elements (Scoped Identifier + Element + Definition/Designation + Slots)
- Described Elements (Namespaces + Definition/Designation

# IMPORTANT

As of September 1st, 2017, the repository has moved to https://bitbucket.org/medicalinformatics/samply.mdr.dal. Please see the following help article on how to change the repository location in your working copies:

    https://help.github.com/articles/changing-a-remote-s-url/

If you have forked Samply.MDR in Bitbucket, the fork is now linked to the new location automatically. Still, you should change the location in your local copies (usually, the origin of the fork is configured as a remote with name "upstream" when cloning from Bitbucket).